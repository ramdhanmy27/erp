<?php 

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Route;

class User extends Authenticatable
{
    protected $connection = 'bcss';
    protected $table      = 'tenant_users';

    private $features;

    protected $fillable = [
        'name', 'mail', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tenant()
    {
        return $this->belongsTo(\App\Models\Tenant::class, 'cif', 'cif');
    }

    public function scopeSearch($query, $value)
    {
        return $query->where("name", "like", "%$value%")
		            ->orWhere("mail", "like", "%$value%");
    }

    /**
     * Get all available user's features
     * @return array
     */
    public function getFeatures($app_path = null) {
        if ($this->features === null) {
            if ($app_path == null) {
                if (APP_MATCHED)
                    $app_path = APP_PATH;
                else
                    throw new \Exception("Application path is undefined");
            }

            $this->features = DB::connection("bcss")->table("app_features as af")
                ->join("tenant_users_auth as ua", "ua.app_feature_id", "=", "af.id")
                ->join("application as app", "app.id", "=", "af.application_id")
                ->where("ua.tenant_user_id", $this->id)
                ->where("app.path", $app_path)
                ->groupBy("af.id")
                ->pluck("af.id");
        }

        return $this->features;
    }

    /**
     * Check User's available features
     * @param  string|array  $feature
     * @return boolean
     */
    public function hasFeatures($feature, $app_path = null) {
        $app_features = $this->getFeatures($app_path);

        if (is_array($feature))
            return count(array_intersect($app_features, $feature)) == count($feature);
        else
            return in_array($feature, $app_features);
    }

    /**
     * User has at least one feature available
     * @param  array  $feature
     * @return boolean
     */
    public function hasOneFeatures(array $feature, $app_path = null) {
        return count(array_intersect($this->getFeatures($app_path), $feature)) > 0;
    }

    public function hasAbility($name, $credentials) {
        if ($data = $this->compareCredentials($name, $credentials))
            return $data->result == $data->credential;

        return false;
    }

    public function hasOneAbility($name, $credentials) {
        if ($data = $this->compareCredentials($name, $credentials))
            return $data->result != "0";

        return false;
    }

    public function compareCredentials($name, $credential) {
        return DB::table("credential")->where([["name", $name], ["feature", Route::getFeature()]])
                ->whereIn("credential", is_array($credential) ? $credential : [$credential])
                ->selectRaw("sum(value) & (select credential_value from users where id=?) as result", [$this->id])
                ->selectRaw("sum(value) as credential")
                ->first();
    }
}
