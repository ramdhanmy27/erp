<?php

namespace App\Models;

class AppRental extends Model
{
    const Active = 1;
	const Inactive = 0;

	protected $connection = 'bcss';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'app_rental';

	public function tenant()
	{
		// return $this->belongsTo('App\Models\Tenant', 'cif', 'cif');
	}

	public function application()
	{
		// return $this->hasOne('App\Models\Application', 'id', 'application_id');
	}
}
