<?php

namespace App\Http\Middleware;

use Closure;
use Flash;
use Auth;
use DB;

use App\Models\AppRental;

class TenantSass
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		$user = Auth::guard(null)->user();

		// $router  = app()->router;
		// $feature = $router->getFeature();

		$query = DB::connection('bcss')->table('application as a');
		$query->join('app_rental as ar', 'ar.application_id', '=', 'a.id')
			  ->leftJoin('app_features as af', 'af.application_id', '=', 'a.id')
			  ->leftJoin('tenant_users_auth as ta', 
					DB::raw('"ta"."app_feature_id"="af"."id" AND '.
							'"ta"."tenant_user_id" = ? '), 
					DB::raw(''), DB::raw(''))
			  ->addBinding([$user->id])
			  ->where('a.path', '=', APP_PATH)
			  ->where('a.release_version', '=', $user->tenant->installed_version)
			  ->where('ar.cif', '=', $user->cif)
			  ->where('ar.end_date', '>=', DB::raw("'" . date('Y-m-d') . "'::date"));

		$access = $query->first();

		if ($access != null)
		{
			if ($access->tenant_user_id == null)
				Flash::danger(trans('messages.unauthorized-app'));
			else if ($access->status != AppRental::Active)
				Flash::danger(trans('messages.inactive-app'));
			else
				return $next($request); // "This way sir !"
		}
		else Flash::danger(trans('messages.unauthorized-app'));

		return redirect('/');
	}
}
