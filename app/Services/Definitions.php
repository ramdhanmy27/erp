<?php

namespace App\Services;
use Closure;

class Definitions {

	private $app;
	private $definitions;

	public function __construct($app) {
		$this->app = $app;
	}

	public function define(Closure $callback) {
		call_user_func($callback, $this);
	}
}