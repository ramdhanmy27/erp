<?php

namespace App\Services\Routing;

use Closure;
use Menu;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Container\Container;
use App\Services\Routing\Router;
use App\Services\Widget\Menu\MenuRouting;

class Router extends \Illuminate\Routing\Router {

	public $feature;

    /**
     * Containing mixed data
     * 
     * @var Object
     */
	private $param;

	public function __construct(Dispatcher $events, Container $container = null) {
        parent::__construct($events, $container);

        $this->routes = new RouteCollection;
        $this->menu = new MenuRouting;
        $this->param = new \StdClass;
    }

	/**
     * Filter route access with feature.
     *
     * @param  string|array  $attr
     * @param  \Closure|array|string|null  $action
     * @return \Illuminate\Routing\Route
     */
	public function feature($attr, Closure $action) {
        $is_arr = is_array($attr);
        $this->feature = $is_arr ? $attr["feature"] : $attr;

        if ($is_arr)
            $this->group($attr, $action);
        else
            call_user_func($action, $this);

        $this->feature = null;

        return $this;
	}

	/**
	 * Retrieve accessing feature
	 * 
	 * @return String
	 */
	public function getFeature() {
		return $this->feature;
	}

    /**
     * Register menu with an url route
     * @param  array        $attr
     * @param  Closure|null $action
     * @return App\Services\Widget\Menu\Factory\Router
     */
    public function menu($attr, Closure $action = null) {
        if (is_array($attr) && isset($attr["feature"])) {
            $this->feature = $attr["feature"];
        }
        else {
            if (is_string($attr))
                $attr = ["title" => $attr];

            $attr["feature"] = $this->feature;
        }

        if ($action === null)
            $menu = Menu::driver("router")->add($attr);
        else 
            $menu = Menu::driver("router")->addGroup($attr, function() use ($attr, $action) {
                $this->group($attr, $action);
            });

        $this->feature = null;

        return $menu;
    }
	
    /**
     * Add a route to the underlying route collection.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  \Closure|array|string  $action
     * @return \Illuminate\Routing\Route
     */
    protected function addRoute($methods, $uri, $action)
    {
        return $this->routes->addItem($this->createRoute($methods, $uri, $action));
    }

	/**
     * Create a new Route object.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  mixed   $action
     * @return \Illuminate\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        return (new Route($methods, $uri, $action))
                    ->setRouter($this)
                    ->setContainer($this->container);
    }

    /**
     * Route a controller to a URI with wildcard routing.
     *
     * @param  string  $uri
     * @param  string  $controller
     * @param  array  $names
     * @return void
     *
     * @deprecated since version 5.2.
     */
    public function controller($uri, $controller, $names = []) {
        parent::controller($uri, $controller, $names);
        $this->menu->setPrefix($this->prefix($uri));

        return $this->menu;
    }

    public function validateDataId($uri) {
        $this->param->uriValidate = [];

        foreach (is_array($uri) ? $uri : func_get_args() as $url)
            $this->param->uriValidate[] = $this->prefix($url);
    }

    public function getParam() {
        return $this->param;
    }
}