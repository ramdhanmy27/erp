<?php 

namespace App\Services\Widget\Menu;

use Menu;
use Closure;
use Route;
use Illuminate\Http\Request;

class MenuRouting {
	
	private $prefix, $lastMenu;

	public function __construct($url = null) {
		$this->setPrefix($url);
	}

	public function setPrefix($url) {
		$this->prefix = $url;
	}

	/**
	 * Used by Route::controller to declare menus
	 * @param  string $title
	 * @param  string $icon
	 * @param  string $url
	 * @param  Closure $isValid
	 * @return MenuRouting
	 */
	public function menu($attr) {
		// next arguments will registered depend on $attr_props value order
		if (!is_array($attr)) {
			$attr_props = ["title", "url", "icon", "isValid"];
			$attr = [];

			foreach (func_get_args() as $i => $value) {
				switch ($attr_props[$i]) {
					case 'url':
						$attr[$attr_props[$i]] = $this->getUrl($value);
						break;
					
					default:
						$attr[$attr_props[$i]] = $value;
						break;
				}
			}
		}
		
		if (!isset($attr["feature"])) {
			$attr["feature"] = Route::getFeature();
		}

		$this->lastMenu = Menu::driver("router")->add($attr);
		
		return $this;
	}

	public function on($callback) {
		if ($this->lastMenu === null)
			return false;

		$this->lastMenu->on($callback);

		return $this;
	}

	protected function getUrl($url = null) {
		return $this->prefix."/".$url;
	}
}