<?php

namespace App\Services\Widget;

class Slider implements WidgetContract {

	const PATH = "img/assets/slider";

	public function getCollection() {
		$files = [];

		foreach (glob(public_path(self::PATH."/*.*")) as $file)
			$files[] = basename($file);

		return $files;
	}

	public function render() {
		return view("ui.slider", [
			"list" => $this->getCollection()
		]);
	}
}