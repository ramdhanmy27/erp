<?php

namespace App\Services;

use App\Models\Model;

class FormBuilder extends \Collective\Html\FormBuilder {

    protected $modelClass, $rules;

    /** Misc */

    public function model($model, array $options = []) {
        // $model is contain namespace class 
        if (is_string($model)) {
            $this->modelClass = $model;

            if (isset($model::$rules))
                $this->rules = $model::$rules;

            // unset model
            $model = null;
        }
        // get rules from model instance
        else $this->rules = isset($model::$rules) ? $model::$rules : null;

        return parent::model($model, $options);
    }

    public function rules($rules) {
        if (!is_array($rules))
            return;

        $this->rules = $rules + $this->rules;
    }

    public function open(array $options = []) {
        return parent::open($options + ["class" => "form-horizontal form-bordered"]);
    }

    public function label($name, $value = null, $options = []) {
        return parent::label($name, $value, $options + ["class" => "col-md-3 control-label"]);
    }

    public function group($type, $name, $title = null, $value = null, $options = []) {
        switch ($type) {
            default:
                $type = str_replace("-", "", $type);

                if (method_exists($this, $type)) {
                    $param = array_slice(func_get_args(), 3);
                    array_unshift($param, $name);
                    
                    $input = call_user_func_array([$this, $type], $param);
                }
                else $input = $this->input($type, $name, $value, $options);
                break;
        }

        return "<div class='form-group'>".
                    $this->label($name, $title===null ? ucwords(preg_replace("/[_\-]/", " ", $name)) : $title)
                    ."<div class='col-md-6'>$input</div>"
                ."</div>";
    }

    public function close() {
        $this->modelClass = null;
        $this->rules = null;

        return parent::close();
    }

    /** Inputs */

    public function input($type, $name, $value = null, $options = []) {
    	return parent::input($type, $name, $value, $this->getInputOptions($name, $options));
    }

    public function date($name, $value = null, $options = []) {
        $options["datepicker"] = "";
        return $this->text($name, $value, $this->getInputOptions($name, $options + ["datepicker" => ""]));
    }

    public function dateRange($name, $value = null, $options = []) {
        return $this->text($name, 
            is_array($value) ? implode(" - ", $value) : $value, 
            $this->getInputOptions($name, $options + ["datepicker-range" => ""])
        );
    }

    public function datetime($name, $value = null, $options = []) {
    	return parent::datetime($name, $value, $this->getInputOptions($name, $options));
    }

    public function time($name, $value = null, $options = []) {
        $options["timepicker"] = "";
        return $this->text($name, $value, $this->getInputOptions($name, $options));
    }

    public function select($name, $list = [], $selected = null, $options = []) {
        return parent::select($name, $list, $selected, $this->getInputOptions(
            $name, $options + ["select2" => "", "placeholder" => "Select.."]
        ));
    }

    public function checkbox($name, $value = 1, $checked = null, $options = []) {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();

        return "<div class='checkbox-custom ".@$options['class']."'>"
                    .parent::checkbox($name, $value, $checked, $this->getInputOptions($name, $options + ["id" => $id]))
                    ."<label for='$id'>".@$options['label']."</label>
                </div>";
    }

    public function checkboxes($name, $options = [], $checked = []) {
        $html = [];

        foreach ($options as $value => $label)
            $html[] = $this->checkbox($name, $value, in_array($value, $checked), ["label" => $label]);

        return implode("", $html);
    }

    public function toggleSwitch($name, $value = 1, $checked = null, $options = []) {
        $options["switch"] = "";

        return "<div class='switch ".@$options["class"]."'>"
                    .parent::checkbox($name, $value, $checked, $options)
                ."</div>";
    }

    public function radio($name, $value = null, $checked = null, $options = []) {
        $id = isset($options["id"]) ? $options["id"] : $name.date("His").rand();

        return "<div class='radio-custom ".@$options['class']."'>"
                    .parent::radio($name, $value, $checked, $this->getInputOptions($name, $options + ["id" => $id]))
                    ."<label for='$id'>".@$options['label']."</label>
                </div>";
    }

    public function radios($name, $options = [], $checked = []) {
        $html = [];

        foreach ($options as $value => $label)
            $html[] = $this->radio($name, $value, in_array($value, $checked), ["label" => $label]);

        return implode("", $html);
    }

    public function textarea($name, $value = null, $options = []) {
        return parent::textarea($name, $value, $this->getInputOptions($name, $options));
    }

    /** Buttons */

    public function submit($value = null, $options = []) {
    	return parent::submit($value, $options + ["class" => "btn btn-primary"]);
    }

    public function button($value = null, $options = []) {
    	return parent::button($value, $options + ["class" => "btn btn-default"]);
    }

    /** Methods */

    public function getFieldRule($name) {
        if ($this->rules == null || !isset($this->rules[$name]))
            return null;

        return $this->rules[$name];
    }

    protected function getInputOptions($name = null, $options = []) {
        $defaultOpt = ["class" => "form-control"];

        if ($rules = $this->getFieldRule($name))
            $defaultOpt["rules"] = $rules;

        return $options + $defaultOpt;
    }
}