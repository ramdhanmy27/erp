<?php 

namespace App\Services;

use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\UserProvider;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SsoGuard extends SessionGuard
{
    private $key;

    /**
     * Create a new authentication guard.
     *
     * @param  \Illuminate\Contracts\Auth\UserProvider  $provider
     * @param  \Symfony\Component\HttpFoundation\Session\SessionInterface  $session
     * @param  \Symfony\Component\HttpFoundation\Request  $request
     * @return void
     */
    public function __construct($key,
                                UserProvider $provider,
                                SessionInterface $session,
                                Request $request = null)
    {
        parent::__construct('sso', $provider, $session, $request);
        $this->key = $key;
    }

    /**
     * Compatible session name with laravel 5.0
     *
     * @return string
     */
    public function getName()
    {
        return 'login_' . md5($this->key);
    }

    /**
     * Compatible recaller name with laravel 5.0
     *
     * @return string
     */
    public function getRecallerName()
    {
        return 'remember_' .md5($this->key);
    }
}