<?php

namespace App\Facades;

class Grid extends \Illuminate\Support\Facades\Facade {
	
	protected static function getFacadeAccessor() {
		return "table.grid";
	}
}