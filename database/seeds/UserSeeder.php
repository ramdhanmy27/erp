<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** Roles */
        $roles = ["admin" => null, "staff" => null];

        foreach ($roles as $name => $val) {
            $role = new Role();
            $role->name = $name;
            $role->display_name = ucwords($name);
            $role->save();

            $roles[$name] = $role;
        }

        /** Users */
        $users = [[
            "name" => "admin",
            "email" => "admin@gmail.com",
            "password" => bcrypt("admin"),
            "role" => "admin",
        ]];

        for ($i=1; $i<=5; $i++)
            $users[] = [
                "name" => str_random(10),
                "email" => str_random(10)."@gmail.com",
                "password" => bcrypt("pass"),
                "role" => "staff",
            ];

        foreach ($users as $data) {
            $user = User::create($data);

            // attach role into user
            if (isset($roles[$data["role"]]))
                $user->attachRole($roles[$data["role"]]);
        }
    }
}
