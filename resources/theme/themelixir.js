var elixir = require('laravel-elixir');

var _ = {
    tmp: {},
    pub: {},
    
    shared: {
        js: "./resources/assets/js/",
        css: "./resources/assets/css/",
    },

    mix: function(theme, callback) {
        if (theme !== "")
            theme = "theme/"+ theme +"/";

        elixir.config.assetsPath = "resources/"+ theme +"/assets/";

        _.tmp = {
            js: "./resources/"+ theme +"assets/js/tmp/",
            css: "./resources/"+ theme +"assets/css/tmp/",
        };

        _.pub = {
            js: "./public/"+ theme +"js/",
            css: "./public/"+ theme +"css/",
        }

        elixir(callback);
    },
};

module.exports = _