@extends('front::home.app')

@section('content')
	@foreach ($carousel as $title => $list)
		<div class="heading heading-border heading-bottom-border">
			<h2>{{ ucwords($title) }}</h2>
		</div>

	    @include("ui.carousel", $list)
	@endforeach
@endsection