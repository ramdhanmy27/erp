<?php

return [

    /**
     * Berisi kalimat-kalimat pesan atau alert.
     */

    'unauthorized-app' => 'Anda tidak memiliki akses.',
    'inactive-app'     => 'Aplikasi tidak dapat digunakan untuk sementara, silahkan hubungi admin.',
];
