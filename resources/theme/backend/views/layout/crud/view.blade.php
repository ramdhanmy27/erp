@extends("backend::app")

@section("title", "View $crud->model_title")

@section("content")
<div class="panel panel-featured panel-featured-primary">
	<div class="panel-heading">
		<h3 class="panel-title">@yield("title")</h3>
	</div>
	
	<div class="panel-body">
		<table class="table table-hover table-bordered table-striped">
			@if (isset($crud->view_field))
				@foreach($crud->view_field as $key => $val)
					<?php $is_assoc = !is_numeric($key); ?>
					<?php $column = $is_assoc ? $key : $val; ?>

					<tr>
						<th>{{ $is_assoc ? $val : id2title($column, "_") }}</th>
						<td>{{ $model->{$column} }}</td>
					</tr>
				@endforeach
			@else
				@foreach($model->getAttributes() as $column => $value)
					<tr>
						<th>{{ id2title($column, "_") }}</th>
						<td>{{ $value }}</td>
					</tr>
				@endforeach
			@endif
		</table>
	</div>
</div>
@endsection